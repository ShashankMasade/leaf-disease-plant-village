argparse==1.1
scipy==1.0.0
sklearn==0.19.1
matplotlib==2.1.2
seaborn==0.8.1
numpy==1.14.1
cv2==3.4.0
pandas==0.22.0
